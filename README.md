# **Global Campaign Optimization** 

I unified data across channels and delivered actionable insights to optimize and empower campaign execution.

## Datasets:
- First Party & Custom Audience Reporting
- Device & Location Reporting
- Inventory Reporting
- Third Party Audience Reporting

